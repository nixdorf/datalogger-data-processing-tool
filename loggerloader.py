import os
import glob
import shutil
import re
import ast
from ftplib import FTP
from zipfile import ZipFile
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import pytz
import hydraulicDia
import random
from matplotlib import rcParams
from Hydrograph.hydrograph import sepBaseflow
from saqc import SaQC, SimpleFlagger

cwd = os.getcwd()

try:
    os.mkdir('output_data/ts')
except FileExistsError:
    pass


class LevelNotFoundError(Exception):
    """Raised when the lower level data is not found"""

    def __init__(self, message="Level data not found."):
        self.message = message
        super().__init__(self.message)


def get_air_pressure(loc, station_id, barologger=True):
    """Downloads air pressure data from DWD and interpolates to 5 min interval."""
    print('retrieving air pressure data from DWD...', end='')
    os.chdir(cwd)
    dwd_url = 'opendata.dwd.de'
    path_pressure = '/climate_environment/CDC/observations_germany/climate/subdaily/pressure/'

    try:
        os.mkdir('dwd')
    except FileExistsError:
        pass

    # establish connection to DWD ftp server
    ftp = FTP(dwd_url)
    ftp.login()
    ftp.cwd(path_pressure)

    ftp.cwd('recent/')
    filename = [station for station in ftp.nlst() if station_id in station][0]
    with open(f'dwd/{filename}', 'wb') as f:
        ftp.retrbinary("RETR " + filename, f.write)

    ftp.cwd('../historical/')
    filename = [station for station in ftp.nlst() if station_id in station][0]
    with open(f'dwd/{filename}', 'wb') as f:
        ftp.retrbinary("RETR " + filename, f.write)
    ftp.quit()

    for file in glob.glob('dwd/*.zip'):
        with ZipFile(file, 'r') as zipObj:
            zipObj.extractall('dwd')
        os.remove(file)

    csv_files = [file for file in sorted(glob.glob(f'dwd/produkt_pp_*'))]

    # concatenate historical and recent data, format dates
    pressure_hist = pd.read_csv(csv_files[0], sep=';')
    pressure_hist.set_index(pd.to_datetime(pressure_hist['MESS_DATUM'], format='%Y%m%d%H'), inplace=True)
    pressure_recent = pd.read_csv(csv_files[1], sep=';')
    pressure_recent.set_index(pd.to_datetime(pressure_recent['MESS_DATUM'], format='%Y%m%d%H'), inplace=True)

    dwd_air_pressure = pd.concat([pressure_hist, pressure_recent])
    dwd_air_pressure = dwd_air_pressure[~dwd_air_pressure.index.duplicated()]
    shutil.rmtree('dwd/')

    pressure_dfs = []
    for num, df in enumerate(loc.logs):
        # round off/up the hours to get valid start and end point for interpolation.
        # e.g. log data starts at 12:25:00 -> start dwd data at 12:00:00, not 18:00:00
        first_time = df.index[0]

        h = first_time.hour
        # round off time for first entry
        if h > 18:
            h = 18
        elif 18 > h >= 12:
            h = 12
        elif 12 > h > 6:
            h = 6
        else:
            h = 0
        first_time = (first_time.replace(second=0, minute=0, hour=h))

        last_time = df.index[-1]
        h = last_time.hour
        # round up time for last entry
        if h > 18:
            h = 0
            last_time += timedelta(days=1)
        elif 18 > h >= 12:
            h = 18
        elif 12 > h >= 6:
            h = 12
        else:
            h = 6
        last_time = last_time.replace(second=0, minute=0, hour=h)

        # create mask from measurement period to extract matching period from dwd data
        mask = (dwd_air_pressure.index >= first_time) & (dwd_air_pressure.index <= last_time)
        pressure_df = dwd_air_pressure.loc[mask]

        pressure_df = pressure_df['PP_TER'].resample('5min').interpolate(method='linear')
        pressure_df = pressure_df.to_frame(name='air_pressure')

        # create mask from measurement period to extract matching period from dwd data
        mask = (pressure_df.index >= df.index[0]) & (pressure_df.index <= df.index[-1])
        pressure_df = pressure_df.loc[mask]

        pressure_dfs.append(pressure_df)

    if barologger:
        barologger = Location('MBaro')
        barologger.get_level1(interpolate=True)
        # Barologger may contain NaNs which have to be filled up with DWD air pressure
        pressure_df = pd.merge(barologger.level1.to_frame(name='baro'),
                               pd.concat(pressure_dfs),
                               how='right', left_index=True, right_index=True).drop_duplicates()
        # calculate the mean difference between barologger and DWD (mainly due to height differences)
        difference = np.nanmean(pressure_df.air_pressure - pressure_df.baro)
        # fill up the missing values
        pressure_df['air_pressure'] = pressure_df['baro'].fillna(pressure_df['air_pressure'] - difference)
        pressure_dfs = [pressure_df.loc[df.index[0]: df.index[-1]] for df in loc.logs]
    return pressure_dfs


def create_header(logger, level):
    # write global metadata

    units = {'level': 'm',
             'temp': 'degC',
             'conductivity': 'µS/cm',
             'hyd_dia': 'm',
             'area': 'm-2',
             'q_1': 'm-3 s-1',
             'q_2': 'm-3 s-1',
             'q_3': 'm-3 s-1',
             'q_4': 'm-3 s-1',
             'q_5': 'm-3 s-1'}

    channels = [f'{"-" * 10}, Channel {enum + 1}: {col}, Unit: {units.get(col)}'
                for enum, col in enumerate(getattr(logger, f'level{level}').columns)]
    channels = [y for x in [x.split(', ') for x in channels] for y in x]

    globe = ['[METADATA]',
             '-' * 50,
             f'Location:  {logger.name}',
             f'Created: {datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")}',
             f'Total number of records: {len(logger.level1)}',
             f'Global Start Time: {logger.metadata[logger.name]["period"][0].strftime("%Y/%m/%d %H:%M:%S")}',
             f'Global End Time: {logger.metadata[logger.name]["period"][1].strftime("%Y/%m/%d %H:%M:%S")}',
             f'''Runtime: {str(pd.to_datetime(logger.metadata[logger.name]["period"][1]) -
                               pd.to_datetime(logger.metadata[logger.name]["period"][0]))}''']
    logmeta = []

    # write metadata for each log
    for i in range(0, len(logger.header)):
        logmeta_i = ['-' * 10,
                     f'Log No {i + 1}',
                     f'Number of records {len(logger.logs[i])}',
                     f'Log Start Time: {logger.header[i]["[Instrument info from data header]"]["Start Time"]}',
                     f'Log Stop Time: {logger.header[i]["[Instrument info from data header]"]["Stop Time"]}',
                     f'Serial No: {logger.header[i]["[Instrument info]"]["Serial number"]}',
                     f'Instrument type: {logger.header[i]["[Instrument info]"]["Instrument type"]}']

        logmeta += logmeta_i

    return globe + channels + logmeta + [f'{"-" * 50}\n[DATA]']


class Location:
    def __init__(self, name):
        self.name = name
        self.files = self.get_files(f'input_data/logs/{self.name}')
        self.logs = []
        self.header = []
        self.level1 = None
        self.level2 = None
        self.level3 = None

        self.heights = None
        self.metadata = {self.name: {'serials': [],
                                     'readouts': [],
                                     'device_change': [],
                                     'period': []}}

    def __len__(self):
        return len(self.logs)

    @staticmethod
    def get_files(filepath):
        for (dirpath, dirnames, filenames) in os.walk(filepath):
            files = sorted([f'{dirpath}/{filename}' for filename in filenames])
        return files

    def combine_dataframes(self, other, level):
        try:
            if level == 1:
                return self.level1.join(other.level1, lsuffix=f'_{self.name}', rsuffix=f'_{other.name}')
            elif level == 2:
                return self.level2.join(other.level2, lsuffix=f'_{self.name}', rsuffix=f'_{other.name}')
            elif level == 3:
                return self.level3.join(other.level3, lsuffix=f'_{self.name}', rsuffix=f'_{other.name}')
        except TypeError:
            raise LevelNotFoundError(message=f'Level {level} not found in one or both locations.')

    def get_level1(self, interpolate=False):
        """Generates level 1 data from logs.
        Level 1:
            - unprocessed values from logs, level in mWS
            - invalid data removed manually, e.g. for times during readout
        """

        def is_dst(dt=None, timezone='Europe/Amsterdam'):
            """
            Checks whether the date is in day save time (Sommerzeit)
            """
            if dt is None:
                dt = datetime.utcnow()
            timezone = pytz.timezone(timezone)
            timezone_aware_date = timezone.localize(dt, is_dst=None)
            return timezone_aware_date.tzinfo._dst.seconds != 0

        def model_check(data, filenum):
            """Checks logger model and corrects units.
                Level: If model is LeveloggerII or Junior, we have to add 9.5 mWS
                Conductivity: If unit is mS/cm we convert to uS/cm"""
            if any(model in self.header[filenum]['[Instrument info]']['Instrument type'] for model in
                   ['LTC_Jr', 'LeveloggerII']):
                data['level'] = data['level'] + 9.5

            try:
                if self.header[filenum]['[CHANNEL 3 from data header]']['Unit'] == 'mS/cm':
                    data['conductivity'] *= 1000
            except KeyError:
                # No Channel 3 available, no conductivity measured
                pass
            return data

        def parse_lev(file):
            """Parses Solinst levelogger .lev file.

            :parameter
            file: str
                path to .lev log file

            :returns
            data: pd.DataFrame
                pandas DataFrame containing log data
            metadata: dict
                dictionary containg information from log header part
            """

            with open(file, 'r', encoding='ISO-8859-1') as f:
                header = []
                serial_flag = False
                for linenum, line in enumerate(f, 1):
                    if '[Data]' not in line:
                        # case 0: [Data] not reached yet, append lines to header
                        header.append(line)
                    else:
                        # case 1: Log data begins, write to pandas dataframe and break line reading
                        data = pd.read_csv(file, skiprows=linenum + 1, skipfooter=1, sep=r"[ ]{1,}", engine='python',
                                           names=['date', 'time', 'level', 'temp', 'conductivity'])
                        break

            # remove artifacts from header lines
            header = [line.rstrip('\n') for line in header]
            header = [line.strip(' ') for line in header]
            header = [re.sub('\s+', ' ', line) for line in header]

            # get parameter names inside square brackets, e.g. [Instument Info] or [Channel 1]
            r = re.compile('\[(.*?)\]')
            parameters = list(filter(r.match, header))

            # store parameter list indices for parsing content
            param_idx = [header.index(param) for param in parameters]

            # create dictionary entry for every parameter
            meta = {param: {} for param in parameters}
            for enum, parameter in enumerate(parameters):

                if enum == len(parameters) - 1:
                    # case 0: for last parameter get content from parameter index until end of header
                    contents = [content.split('=') for content in header[param_idx[enum] + 1:]]
                else:
                    # case 1: for other parameters get content between two parameter indices
                    contents = [content.split('=') for content in header[param_idx[enum] + 1: param_idx[enum + 1]]]
                # remove leading and trailing spaces
                contents = [[i.strip(' ') for i in j] for j in contents]

                # iterate over all parameter contents and convert them to key-value pairs
                pairs = []
                for content in contents:
                    if len(content) > 2:
                        # case 0: Content contains more than one '=' and is split into more than two elements
                        pair = [content[0], ''.join(content[1:])]
                    elif content[0] == '':
                        # case 1: Content has more than 1 line, merge value to prior key and continue instead of append
                        pairs[-1][-1] = content[-1]
                        continue
                    else:
                        # case 2: Default case, no change required
                        pair = content
                    pairs.append(pair)

                # write contents as key-value pair to metadata dict for every parameter
                for pair in pairs:
                    meta[parameter].update({pair[0]: pair[1]})
            meta['[Instrument info]']['Serial number'] = re.search('-(.*) ',
                                                                   meta['[Instrument info]']['Serial number']).group(1)

            return data, meta

        # read data from files
        for filenum, file in enumerate(self.files):
            df, meta = parse_lev(file)
            self.header.append(meta)
            df = model_check(df, filenum)
            self.logs.append(df)

        period = []
        for i, df in enumerate(self.logs):
            # index dfs by datetime
            df['datetime'] = df['date'].map(str) + ' ' + df['time']
            df['datetime'] = pd.to_datetime(df['datetime'], format='%Y%m%d %H:%M:%S')
            # check whether the first value is summer time or not, because time is synchronized with first value
            if is_dst(dt=df['datetime'].iloc[0]):
                df['datetime'] = df['datetime'] - timedelta(hours=2)
            else:
                # is winter time
                df['datetime'] = df['datetime'] - timedelta(hours=1)
            # clean dataframe
            df = df.drop(columns=['date', 'time'], axis=1)
            df = df.set_index('datetime')

            # save time of device change in case of different serial no
            self.metadata[self.name]['serials'] = [header['[Instrument info]']['Serial number']
                                                   for header in self.header]
            if i < len(self.logs) - 1:
                if self.metadata[self.name]['serials'][i + 1] != self.metadata[self.name]['serials'][i]:
                    self.metadata[self.name]['device_change'].append(df.index[-1])
            # save time of data readouts
            self.metadata[self.name]['readouts'].append(df.index[-1])

            # save measuring period

            if i == 0:
                period.append(df.index[0])
            if i == len(self.logs) - 1:
                period.append(df.index[-1])

            if interpolate:
                if (df.index[1] - df.index[0]).seconds / 60 < 5:
                    df = df.resample('5min').interpolate(limit=5)
                else:
                    df = (df.resample('5min').mean()).resample('5min').interpolate(limit=5)
                print('input data interpolated to interval')
            self.logs[i] = df
        # round first and last measurement time to 5 minute interval
        if period[0].minute % 5 != 0 or period[0].second != 0:
            period[0] = period[0] - timedelta(seconds=(period[0].minute * 60 + period[0].second % 300))
        if period[1].minute % 5 != 0 or period[1].second != 0:
            period[1] = period[1] + timedelta(seconds=(300 - (((period[1].minute * 60) % 300) + period[1].second)))
        self.metadata[self.name]['period'] = period

        self.level1 = pd.concat(self.logs)
        self.level1 = self.level1[~self.level1.index.duplicated()]
        idx = pd.date_range(self.level1.index[0], self.level1.index[-1], freq='5min')
        self.level1 = self.level1.reindex(idx)
        self.level1.index.name = 'time'

        # special case if Location is Barologger for barocompensation
        if self.name == 'MBaro':
            self.level1.drop(['conductivity'], axis=1, inplace=True)
            # if pressure lower than 50, then levelloggerII measures mWS, convert to kPa
            self.level1.loc[self.level1['level'] < 50] *= 9.81

            # convert form kPa to hPa
            self.level1['level'] = self.level1['level'] * 10

            # Fill up the missing measurements in barologger time row
            self.level1 = self.level1['level']

    def get_level2(self, barologger=True, correct=False, ec_clean=False, station_id='01048'):
        """
        Generates level 2 data from level 1 data. Calculates water levels.

        Parameters
        ----------
        station_id: str, optional
            ID of DWD weather station which should be used for baro compensation. Default is Dresden-Klotzsche.
        barologger: bool, optional
            If True Barologger log data will be used for baro compensation.
        correct: bool
            If True following corrections are applied:
               - height correction
                 height under logger are added
                - zero correction
                  measurement error is corrected, based on air pressure: If logger is outside of water, it should
                  measure air pressure. Therefor water level has to be 0, since the logger measures
                  air pressure + hydrostatic pressure
        ec_clean: bool
            If True,  electric conductivity is used to identify periods where logger is dry. This is required
            since time when measurement site is dry is unkown
        """

        def calc_level(dwd_dfs, logs, barologger):
            """calculates level from raw sensor pressure data"""
            if barologger:
                baro_deviation = 0
            else:
                # 6.3585: mean deviation between barologger and dwd station
                baro_deviation = 6.3585

            print('calculating levels...', end='')
            for dwd_df, df in zip(dwd_dfs, logs):

                pressure_water = (1000 * 9.81 * df['level']) - ((dwd_df['air_pressure'] - baro_deviation) * 100)
                # h = p_water / (roh * g)
                df['level'] = pressure_water / (1000 * 9.81)
            print('OK')
            return logs

        def correction(loc, dfs, meta):
            """adds height under logger and zero correction to level"""
            cor = pd.read_csv('input_data/corrections.txt', sep='\t')
            h = pd.read_csv('input_data/heights.txt', sep='\t')
            cor = pd.Series(cor.correction.values, cor.serial.values).to_dict()
            h = pd.Series(h.height.values, h.stream.values).to_dict()
            # add zero correction for each device/log
            for i, (df, serial) in enumerate(list(zip(dfs, meta.get('serials')))):
                try:
                    dfs[i]['level'] += cor.get(serial)
                    print('correct level for logger nr', serial)
                except:
                    print(f'{serial}: No Explicit correction given, 0 is assumed')
                    dfs[i]['level'] += 0
                    pass

            # add height under logger for each log
            # literal_eval to read list from file: https://stackoverflow.com/q/1894269/
            for i, (df, height) in enumerate(list(zip(dfs, ast.literal_eval(h.get(loc))))):
                dfs[i]['level'] += height
            return dfs

        def clean(dfs, tresh=10):
            """Eliminates poor data by EC threshold. Sets level to 0 and temperature to NaN.
               We make the assumption that EC < threshold means the logger is dry. This provides an easy method
               for the identification of dry periods in small streams, since we do not know what happens in the
               'dead space' below the logger. However this method is questionable, since low EC could also have
               other reasons like a silted logger.
            """
            # In case of dry out (conductivity<thresh) set level and temp NaN,
            # because air temp is logged instead of water temp
            for df in dfs:
                df.loc[df.conductivity < tresh, 'level'] = 0
                df.loc[df.conductivity < tresh, 'temp'] = np.NaN

            return dfs

        def shift_timerow():
            """Placeholder for shifting part of time row up or down, including discussion"""
            pass

        air_pressure = get_air_pressure(self, station_id=station_id, barologger=barologger)
        level2 = calc_level(air_pressure, self.logs, barologger)

        if correct:
            level2 = correction(self.name, level2, self.metadata[self.name])

        if ec_clean:
            level2 = clean(level2)

        self.level2 = pd.concat(level2)
        self.level2.loc[self.level2.level < 0, 'level'] = 0
        self.level2 = self.level2[~self.level2.index.duplicated()]
        idx = pd.date_range(self.level2.index[0], self.level2.index[-1], freq='5min')
        self.level2 = self.level2.reindex(idx)
        self.level2.index.name = 'time'

    def get_level3(self, force_b=False):
        """Generates level 3 data based on level 2 data.
            Level 3:
                - Discharges based on Manning-Strickler-Formula
                - Since Strickler constant k_st and slope are difficult to measure, we assume the product of both (k_B)
                  is constant for every location. Based on manual flow measurements on-site, we estimated a k_B for
                  every measurement. Therefor multiple possible flows q_1 - q_n are calculated
                - calculation of hydraulic diameter and cross sectional area of water body are calculated with
                  hydraulicDia.py, which uses profile data measured on-site
                """

        def calc_strickler_B(flow, area, hyd_dia):
            """Calculates constant B = k_st*I^(1/2) based on flow measurement

            :parameter
            flow: float
                Manually measured discharge in m3 s-1
            area: float
                cross-sectional area of water body, calculated by hydraulicDia
            hyd_dia: float
                hydraulic diameter,calculated by hydraulicDia

            :returns
            k_b: float
                Constant B = k_st*I^(1/2) for discharge calculations
            """

            k_b = flow / (area * hyd_dia ** (2 / 3))
            return k_b

        def calc_strickler_f(k_b, area, hyd_dia):
            """Calculates discharge by modified Manning-Strickler-Formula based on constant.

            :parameter
            k_b: float
                Constant B = k_st*I^(1/2) for discharge calculations
            area: float
                cross-sectional area of water body, calculated by hydraulicDia
            hyd_dia: float
                hydraulic diameter,calculated by hydraulicDia

            :returns
            f: float
                calculated flow m3 s-1
            """
            f = (k_b * (hyd_dia ** (2 / 3))) * area
            return f

        if not isinstance(self.level2, pd.DataFrame):
            raise LevelNotFoundError(message=f'Level 2 data not found. Run {self.name}.get_level(2) method first.')

        try:
            flows = pd.read_csv(f'input_data/flow_measurements/{self.name}_flow.csv')
        except FileNotFoundError:
            print('Flow measurement file does not exist. In order to calculate level 3 data, please provide csv '
                  'with manually measured flows in the formate "time,discharge(m s-1)')
            raise

        df = hydraulicDia.calc_diameter(self.name, self.level2)

        print('Calibrating Manning-Strickler-Formula...')
        # prepare table for manually measured flows for calibration of manning-strickler-formular
        # remove rows that to not contain time in datetime by checking length
        flows = flows[flows['time'].map(len) > 10]

        # set datetime index
        flows['datetime'] = pd.to_datetime(flows['time'])
        flows.drop(['time'], axis=1, inplace=True)
        flows.set_index('datetime', inplace=True)

        # find the water levels at time of flow measurement
        level_at_measurement = []
        new_indices = []
        for i in range(0, len(flows)):
            # level at time i
            try:
                level_i = self.level2.loc[flows.index[i]].level
            except:
                print('Flow Measurement on ', flows.index[i], 'not recorded in logger data')
                continue

            deviation = 0
            # when water level is NaN, flow was measured when logger was not in water. Increment by 5 minutes until
            # valid water level measurement is found with deviation of max. 120 minutes
            while np.isnan(level_i):
                level_i = self.level2.loc[flows.index[i] + pd.to_timedelta(f'{str(deviation)} min')].level
                deviation += 5
                if deviation > 120:
                    break
                continue

            if deviation != 0 and deviation < 120:
                print(f'Warning: Level data for timestamp {flows.index[i]} not available. Used next valid measurement '
                      f'with deviation of {deviation} minutes.')
                # Create new datetime index in case level with deviating time had to be used
            new_indices.append(flows.index[i] + pd.to_timedelta(f'{deviation} min'))
            level_at_measurement.append(level_i)

        # recreate table with flevels
        flows = pd.DataFrame({'level': level_at_measurement, 'discharge': flows['discharge(m s-1)'].values},
                             index=new_indices)
        flows = flows[flows['level'].notna()]
        # with matching level, calculated cross-sectional area and hydraulic diameter, calculate constant B for every
        # flow measurement in flows
        flows['const_b'] = calc_strickler_B(flows['discharge'],
                                            df['area'].loc[flows.index],
                                            df['hyd_dia'].loc[flows.index])
        print('Estimated constants for Manning-Strickler:')
        print(flows.const_b)
        for idx in flows.index:
            print(df.loc[idx])
        if force_b:
            print(f'Manually set constant:\n{force_b}')
        # for every constant B 1-n calculate the discharge q_1 - q_n
        for rownum in range(0, len(flows)):
            df[f'q_{rownum + 1}'] = calc_strickler_f(flows.iloc[rownum]['const_b'],
                                                     df['area'],
                                                     df['hyd_dia'])
        if force_b:
            df[f'q_forced'] = calc_strickler_f(force_b,
                                               df['area'],
                                               df['hyd_dia'])
        self.level3 = df

    def get_water_balance(self, discharge_col='q_1',
                          rain_ds_location='path_to_file',
                          evt_ds_location='path_to_file',
                          initial_soilwater_content=None,
                          ):

        """
        A function which computes the missing components of water balance for the
        catchment where the logger is located


        Parameters
        ----------
        sel : TYPE
            DESCRIPTION.
        rain_ds_location : String, optional
            DESCRIPTION. The default is 'path_to_file'.
        evt_ds_location : String, optional
            DESCRIPTION. The default is 'path_to_file'.
        initial_soilwater_content : FLOAT, optional
            DESCRIPTION. The default is False.

        Returns
        -------
        None.

        """

        # first load the area
        areas = pd.read_csv(f'input_data/level4_ancillary_datasets/basin_area/basin_area.csv')
        basin_area = areas.loc[areas.basin == self.name, 'area']

        # collect the data
        df_hydro = pd.DataFrame()
        # raindata_first

        # basin_area=int(pd.read_csv('.\\Input\\basin_rainfall\\'+basin_rain,skiprows=2,nrows=1,sep=':',header=None)[1])
        if 'dwd_station' in rain_ds_location:
            df_rain = pd.read_csv(rain_ds_location)
            df_rain = df_rain.reset_index(drop=True).set_index(pd.to_datetime(df_rain['time']), drop=True)
            df_rain.drop(columns=['time'], inplace=True)
        else:
            df_rain = pd.read_csv(rain_ds_location, skiprows=3)
            df_rain = df_rain.reset_index(drop=True).set_index(pd.to_datetime(df_rain['Time[yymmddhh]'],
                                                                              format='%y%m%d%H%M'), drop=True)
            df_rain.drop(columns=['Time[yymmddhh]'], inplace=True)

        df_rain_daily = df_rain.resample('D').sum()
        df_hydro = pd.concat([df_hydro, df_rain_daily], axis=1)

        # next comes et
        df_ET = pd.read_csv(evt_ds_location, index_col='time')
        df_ET.index = pd.to_datetime(df_ET.index)
        df_ET = df_ET.resample('D').sum()
        df_ET = df_ET.rename(columns={'0': 'ET_MODIS16'})
        df_hydro = pd.concat([df_hydro, df_ET], axis=1)

        df_discharge_daily = self.level3[discharge_col].sort_index().resample('D').mean()
        df_runoff_daily = df_discharge_daily * 24 * 3600 * 1000 / basin_area.values  # m³/s to l/d
        df_runoff_daily.name = 'runoff'
        # impute missing values by mean
        print('interpolate daily nans by mean of time series, because nan covers large areas')
        df_runoff_daily = df_runoff_daily.fillna(df_runoff_daily.mean())
        # calculate daily values
        df_hydro = pd.concat([df_hydro, df_runoff_daily], axis=1)

        # drop rows with nan
        df_hydro = df_hydro.dropna()
        # rename
        df_hydro.index.name = 'time'
        # water budget change
        df_hydro['delta_water'] = df_hydro['rainfall[mm]'] - df_hydro['runoff'] - df_hydro['ET_MODIS16']
        # accumulate water
        df_hydro['delta_water_acc'] = df_hydro.delta_water.cumsum()

        # Compute the soil_water
        # We do the soil water content correction for the non Mueglitz
        # first we check where the daily EC turns to be smaller 5
        df_ec_daily = self.level3.conductivity.resample('D').mean()
        # find first time step where soil reservoir is empty because no runoff
        if initial_soilwater_content is None:
            print('calculate soil water dynamic from dry positions')
            if len(df_ec_daily[df_ec_daily < 10]) > 0:
                time_empty = df_ec_daily[df_ec_daily < 10].idxmin()
                # set the soil water of this timestep to 0 and sum up from there in both directions
                df_soil_prior = -1 * df_hydro['delta_water'].loc[:time_empty].sort_index(ascending=False).cumsum()
                df_soil_after = df_hydro['delta_water'].loc[time_empty + timedelta(days=1):].sort_index().cumsum()
                df_hydro['soil_water'] = pd.concat([df_soil_prior, df_soil_after])
            else:
                raise ValueError('Logger didnt fell dry, please define initial_soil_water content')
        else:
            print('Calculate soil water based on defined initial conditions')
            df_hydro['soil_water'] = initial_soilwater_content + df_hydro['delta_water_acc']

        # add to class
        self.water_balance = df_hydro

    def write(self, filepath, level, header=True):
        # remove level csv to avoid appending to old data
        try:
            os.remove(filepath)
        except FileNotFoundError:
            pass

        if header:
            header_text = create_header(self, level)
            with open(filepath, 'a') as fout:
                for line in header_text:
                    fout.write(f'{line}\n')

        with open(filepath, 'a') as fout:
            (getattr(self, f'level{level}')).to_csv(fout, index=True, line_terminator='\n')

    def quickplot(self, level, period=False, output=False, flagmad=False, window="30d", z=3.5):
        """
        Creates an overview plot for the given level.

        Parameters
        ----------
        level: int
            Level to plot [1, 2, 3]
        period: list of str, optional
            Takes list of two timestamps if only certain period should be plotted. timestamp format is 'YYYY-mm-dd'-
        output: bool, optional
            If True, plot gets stored to disk as PNG.
        flagmad: bool, optional
            Only level 1: If True, outliers will be detected and flagged red in plot.
        window: str, optional
            Only required if flagmad is True. Sets the sliding window for statistics.
        z: float, optional
            Only required if flagmad is True. Sets the z-score for statistics.
        """

        # the tablue20 color set looks so much nicer than mpl default
        tableau20 = [(31, 119, 180), (174, 199, 232), (255, 127, 14), (255, 187, 120),
                     (44, 160, 44), (152, 223, 138), (214, 39, 40), (255, 152, 150),
                     (148, 103, 189), (197, 176, 213), (140, 86, 75), (196, 156, 148),
                     (227, 119, 194), (247, 182, 210), (127, 127, 127), (199, 199, 199),
                     (188, 189, 34), (219, 219, 141), (23, 190, 207), (158, 218, 229)]
        # translate rgb to fractions of 1 so mpl will understand
        for i in range(len(tableau20)):
            r, g, b = tableau20[i]
            tableau20[i] = (r / 255., g / 255., b / 255.)

        if level == 3:
            data = self.level3.copy()
            title = f'{self.name} level 3'
            columns = [col for col in data.columns if 'q_' in col]
            # if period argument was specified, select that period of data
            if period:
                data = data.loc[period[0]: period[1]]
            fig, ax = plt.subplots(figsize=(18, 9))
            fig.suptitle(title)
            ax.set_ylabel(r'Discharge ($\rm m^{3}/s$)')
            for enum, col in enumerate(columns):
                ax.plot_date(data.index, data[col], label=f'Discharge {col}', ls='-', lw=0.2, marker=None,
                             color=tableau20[enum * 2])
            ax.set_ylim(0)
            plt.legend()

            if output:
                plt.savefig(f'output_data/plots/{self.name}_level{level}.png')
            else:
                plt.show()

        else:
            # check level to plot
            if level == 1:
                data = self.level1.copy()
                title = f'{self.name} level 1'
                levelunit = 'mWS'
            elif level == 2:
                data = self.level2.copy()
                title = f'{self.name} level 2'
                levelunit = 'm'
                try:
                    # read in manually measured levels for plot control
                    self.heights = pd.read_csv(f'input_data/level_control/{self.name}_levels.csv')
                    self.heights = self.heights.set_index(pd.to_datetime(self.heights['date'])).drop(['date'], axis=1)

                    # append one day to time series to display level outside of measuring period
                    data = data.append(pd.DataFrame({'level': np.nan, 'conductivity': np.nan, 'temperature': np.nan},
                                                    index=[pd.to_datetime(data.index[-1] + pd.to_timedelta('1 day'))]))

                except FileNotFoundError:
                    pass

            # if period argument was specified, select that period of data
            if period:
                data = data.loc[period[0]: period[1]]

            # create plot with 3 subplots for level, conductivity and temperature
            fig, axs = plt.subplots(3, 1, sharex=True, figsize=(18, 9))
            fig.suptitle(title)

            axs[0].title.set_text('Water level')
            axs[0].set_ylabel(f'Level ({levelunit})')
            axs[0].plot_date(data.index, data.level, label=f'Level ({levelunit})', ls='-', lw=0.2, marker=None,
                             color=tableau20[0])
            for day in [readout for readout in self.metadata[self.name]['readouts'] if readout > data.index[0]]:
                axs[0].axvline(x=day, color='darkorange', linestyle='-', linewidth=0.5)

            # plot manually measured heights if argument was passed, estimated measurement error: +- 3 cm
            if isinstance(self.heights, pd.DataFrame) and level == 2:
                axs[0].errorbar(x=self.heights.index, y=self.heights.level / 100, yerr=0.03, ecolor='red', ls='',
                                capsize=4)
            axs[1].title.set_text('Electric conductivity')
            axs[1].set_ylabel('Electric conductivity [uS]')
            axs[1].plot_date(data.index, data.conductivity, label='EC [uS]', ls='-', lw=0.2, marker=None,
                             color=tableau20[4])

            axs[2].title.set_text('Temperature')
            axs[2].set_ylabel('Temperature [°C]')
            axs[2].plot_date(data.index, data.temp, label='Temp. [°C]', ls='-', lw=0.2, marker=None,
                             color=tableau20[2])

            # mark the outliers in plot if flagmad is True
            if level == 1 and flagmad:
                outliers = outlier_detection(self, window, z)
                for n, ax in enumerate(axs):
                    s = outliers[outliers.columns[n + 3]].cat.codes.where(
                        outliers[outliers.columns[n + 3]].cat.codes != 2,
                        outliers[outliers.columns[n]]).replace(0, np.nan)
                    ax.plot_date(s.index, s,
                                 color='red', ls='-', marker=None)
            for ax in axs:
                ax.set_ylim(0)
            if output:
                plt.savefig(f'output_data/plots/{self.name}_level{level}.png')
            else:
                plt.show()

    # %% next it comes plot for water balance
    def plot_water_balance(self, interval='daily', output=False):
        """
        Make beautiful water balance plots

        Parameters
        ----------
        df_hydro : TYPE
            DESCRIPTION.
        interval : TYPE, optional
            DESCRIPTION. The default is 'daily'.

        Returns
        -------
        None.

        """

        # define a plotting standard
        def setupMatplotlib(height=8., width=8.):
            try:
                plt.close()
            except:
                pass
            plt.rcParams['xtick.direction'] = 'out'
            plt.rcParams['ytick.direction'] = 'out'
            plt.rcParams['lines.linewidth'] = 1.5
            plt.rcParams['lines.color'] = 'black'
            plt.rcParams['legend.frameon'] = False
            plt.rcParams['font.family'] = 'serif'
            plt.rcParams['legend.fontsize'] = 10
            plt.rcParams['font.size'] = 10
            # For ipython notebook display set default values.
            plt.rcParams['lines.markersize'] = 1
            plt.rcParams['figure.figsize'] = (height, width)
            plt.rcParams['grid.linewidth'] = 1
            # General settings used by display and print contexts.
            # plt.rcParams['axes.axisbelow'] = True
            grid_line_color = '0.5'
            plt.rcParams['grid.color'] = grid_line_color
            plt.rcParams['grid.linestyle'] = '-'

        # color definitions
        colum_color = {'runoff': 'navy',
                       'rainfall[mm]': 'aqua',
                       'ET_MODIS16': 'red',
                       'delta_water': 'black',
                       'delta_water_acc': 'black',
                       'soil_water': 'peru'}
        # check which aggregation level
        if interval == 'daily':

            titles = {'runoff': 'Daily runoff',
                      'rainfall[mm]': 'Daily rainfall',
                      'ET_MODIS16': 'Daily evapotranspiration',
                      'delta_water': 'Daily water balance',
                      'delta_water_acc': 'Water balance accumulated',
                      'soil_water': 'Available soil water for drainage'}

            # setupMatplotlib(10,8)
            setupMatplotlib(12, 8)
            fig, axes = plt.subplots(nrows=3, ncols=2, sharex=True, figsize=(12, 8))
            window_order = ((0, 0), (0, 1), (1, 0), (1, 1), (2, 0), (2, 1))
            rgb = (random.random(), random.random(), random.random())
            i = 0
            for column in self.water_balance.columns:
                self.water_balance[column].plot(ax=axes[window_order[i]], c=colum_color[column])
                axes[window_order[i]].set_title(titles[column])
                axes[window_order[i]].set_ylabel('mm/m$^2$')
                axes[window_order[i]].grid(False)
                if i < 3:
                    axes[window_order[i]].text(datetime(2019, 9, 1, 0, 0), self.water_balance[column].max() * 0.8,
                                               'Sum = ' + str(round(self.water_balance[column].sum(), 2)) + 'mm')
                i = i + 1
                rcParams['font.size'] = 10
                rcParams['xtick.labelsize'] = 8
            fig.suptitle('Daily Water Balance for ' + self.name + ' Basin', fontsize=16)

        if interval == 'monthly':
            # next it comes to quickplot features -->MONTHLY

            # %%
            df_hydro_sel_monthly = self.water_balance.resample('M').sum()
            # for soil water we need to use mean instead of sum as accumulation does not make much sense
            df_hydro_sel_monthly['soil_water'] = self.water_balance['soil_water'].resample('M').mean()
            df_hydro_sel_monthly['delta_water_acc'] = df_hydro_sel_monthly.delta_water.cumsum()
            df_hydro_sel_monthly['soil_water'] = self.water_balance['soil_water'].iloc[0] + df_hydro_sel_monthly[
                'delta_water_acc']
            titles = {'runoff': 'Monthly runoff',
                      'rainfall[mm]': 'Monthly rainfall',
                      'ET_MODIS16': 'Monthly evapotranspiration',
                      'delta_water': 'Monthly water balance',
                      'delta_water_acc': 'Water balance accumulated',
                      'soil_water': 'Available soil water for drainage'}

            setupMatplotlib(12, 8)
            fig, axes = plt.subplots(nrows=3, ncols=2, sharex=True, figsize=(12, 8))
            window_order = ((0, 0), (0, 1), (1, 0), (1, 1), (2, 0), (2, 1))
            i = 0
            df_hydro_sel_monthly.index = pd.to_datetime(df_hydro_sel_monthly.index).strftime('%m-%Y')
            for column in df_hydro_sel_monthly.columns:
                df_hydro_sel_monthly[column].plot.bar(ax=axes[window_order[i]], facecolor=colum_color[column])
                axes[window_order[i]].set_title(titles[column])
                axes[window_order[i]].set_ylabel('mm/m$^2$')
                axes[window_order[i]].grid(False)
                if i < 3:
                    axes[window_order[i]].set_ylim(0, 150)
                    axes[window_order[i]].text(6, 100,
                                               'Sum = ' + str(round(df_hydro_sel_monthly[column].sum(), 2)) + 'mm')
                i = i + 1

            fig.suptitle('Monthly Water Balance for ' + self.name + ' Basin', fontsize=16)
        # finally the options for output
        if output:
            plt.savefig(f'output_data/plots/{self.name}_water_balance_{interval}.png', dpi=300)
            plt.close()
        else:
            plt.show()

    # Calculate baseflow
    def flow_separation(self, discharge_col=['q_1'], dt=5, tp_min=1, dt_max=12, k=0.000546):
        """


        Parameters
        ----------
        discharge_col: list of str, optional
            Column on which flow seperation should be performed. The default is ['q_1']
        dt : TYPE, optional
            DESCRIPTION. The default is 5min.
        tp_min : TYPE, optional
            DESCRIPTION. The default is 24.
        dt_max : TYPE, optional
            DESCRIPTION. The default is 12.
        k : TYPE, optional
            DESCRIPTION. The default is 0.000546.

        Returns
        -------
        df_out : TYPE
            DESCRIPTION.

        """

        # first load the area
        areas = pd.read_csv(f'input_data/level4_ancillary_datasets/basin_area/basin_area.csv')
        basin_area = areas.loc[areas.basin == self.name, 'area'] / 1000 / 1000

        # compute discharge to m3/s and resample to 5 min
        df_discharge_int = self.level3[discharge_col].resample(str(dt) + 'Min').mean().interpolate('nearest') / 1000
        df_discharge_int = df_discharge_int.rename(columns={discharge_col[0]: 'discharge [m^3 s^-1]'})
        df_flowsep = sepBaseflow(df_discharge_int, dt, basin_area, tp_min=tp_min, dt_max=dt_max, k=k)
        return df_flowsep


def outlier_detection(logger, window="30d", z=3.5):
    """
    Wrapper function for SaQC module outlier detection. Flags outliers using the simple median absolute deviation test

    Parameters
    ----------
    logger: loggerloader.Location
        Location object containg level 1 data
    window: str, optional
        Sliding window for period to run statistic on, default is "30d" for 30 days
    z: float, optional
        z-parameter of the modified Z-Score, default is 3.5

    Returns
    -------
    flags: pd.DataFrame
    """

    saqc = (SaQC(SimpleFlagger(), logger.level1)).spikes_flagMad(logger.level1.columns[0], window=window, z=z)
    data, flagger = saqc.getResult()
    flags = flagger.getFlags().to_df()
    flags.columns = [f'{col}_FLAG' for col in flags.columns]
    flags = pd.concat([logger.level1, flags], axis=1)
    return flags
