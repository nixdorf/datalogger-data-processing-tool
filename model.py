import os
import loggerloader as ll
from saqc import SaQC

# level 1: concatenated raw data of .lev log files
# level 2: calculated water levels (m)
#          barologger: If True, Barologger is used (dir name: "MBaro"), else DWD is used
#          correct: apply level correction from input_data/corrections.txt
#          ec_clean: set level to zero if EC < 10
# Level 3: calculated discharge based on Manning-Strickler

for location in ['Ostbach', 'Dittersdorf', 'Kohlbach', 'Trebnitz', 'Westbach']:
    logger = ll.Location(location)
    logger.get_level1(interpolate=True)

    """This is an example on how to use the System for automated Quality Control (SaQC) on the level 1 data. For more 
    information please refer to official documentation on https://rdm-software.pages.ufz.de/saqc/index.html"""

    # initialize saqc on level1 data of location object
    qc = SaQC(data=logger.level1, scheme="simple")

    # flag constant data values, values regarded as outliers due to the grubbs test, and values between min and max
    qc = (qc
          .flagConstants("level", thresh=0.01, window="3D")
          .flagByGrubbs("level", window="10D")
          .flagRange("level", min=9.8, max=10.3))

    # return data and flags from SaQC object
    print(qc.data)
    print(qc.flags)

    # plot flagged data
    qc.plot("level")


    # logger.quickplot(level=1, flagmad=True)


    # logger.write(f'output_data/ts/{location}_l1.csv', level=1)
    logger.get_level2(barologger=True, correct=True, ec_clean=False)
    logger.quickplot(level=2, output=True)
    # logger.write(f'output_data/ts/{location}_l2.csv', level=2)
    # level 3 may take a couple of minutes
    # logger.get_level3()

    """
    # get water balance
    logger.get_water_balance(discharge_col='q_1',
                             rain_ds_location=os.path.join('input_data',
                                                           'level4_ancillary_datasets',
                                                           'basin_rainfall',
                                                           'dwd_station',
                                                           'Dippoldiswalde_daily.csv'),
                             evt_ds_location=os.path.join('input_data',
                                                          'level4_ancillary_datasets',
                                                          'basin_ET',
                                                          'ET_' + location + '.csv'),
                             initial_soilwater_content=None
                             )
    # plot water balance
    logger.plot_water_balance(interval='monthly', output=True)
    """
    # logger.flow_separation(discharge_col=['q_1'],dt=5,tp_min=1,dt_max=12,k=0.000546)

    # logger.write(f'output_data/ts/{location}_l3.csv', level=3)

    ## Quick plot with level, EC, Temp for level 1 or 2
    # logger.quickplot(level=1)
    # logger.quickplot(level=2)

    # clip the plot to a time period
    # logger.quickplot(level=1, period=['2020-01-01', '2020-10-31'])

    # Combine two locations for comparison, returns DataFrame, not plottable yet
    # combined_df =  combine_dataframes(LocationObject1, LocationObject2, level=1)

