import pandas as pd
from solinst_load import read_data, get_dwd_pressure
import numpy as np
import matplotlib.pyplot as plt


# read barologger logs and get pressure from DWD
baro, meta = read_data('logs/MBaro', interp=True)
dwd = get_dwd_pressure(baro)

baro = pd.concat(baro)
baro.drop(['conductivity'], axis=1, inplace=True)
dwd = pd.concat(dwd)


# if pressure lower than 50, then levelloggerII measures mWS, convert to kPa
baro.loc[baro['level'] < 50]*=9.81

# convert form kPa to hPa for comparison with DWD
baro['level'] = baro['level']*10

# Fill up the missing measurements in barologger time row
idx = pd.date_range(baro.index[0], baro.index[-1], freq='5min')
# drop duplicates for reindexing
baro = baro[~baro.index.duplicated()]
baro.reindex(idx)


baro['air_pressure'] = baro['level']
# calculate the mean deviation between barologger and dwd: -6.66841
np.mean(baro.level-dwd.air_pressure)

# if we look now at the specific period where Dittersdorf is weird, we have a deviation of -7.47
np.mean(baro.loc['2020-05-28':'2020-07-31']['level']-dwd.loc['2020-05-28':'2020-07-31']['air_pressure'])

# plot barologger against DWD
plt.plot_date(baro.index, baro.level, color='blue', ls='-', lw='0.3', marker=None)
plt.plot_date(dwd.index, dwd.air_pressure, color='red', ls='-', lw='0.3', marker=None)
#plt.show()

# Now let's compare water levels calculated with barologger and DWD
pressure_dfs = []
dfs, meta = read_data(f'logs/Westbach', interp=None)
pd.concat(dfs).level.plot()
dfs = pd.concat(dfs).resample('5min').mean()

west = pd.concat(dfs)
plt.plot_date(pd.DataFrame(west.loc['2020-09-15':'2020-10-03']).index, pd.DataFrame(west.loc['2020-09-15':'2020-10-03']).conductivity, ls='-', lw=0.3, marker=None, label='Westbach')
plt.plot_date(pd.DataFrame(baro['air_pressure'].loc['2020-05-15':'2020-10-03']/9.81/10).index,
              pd.DataFrame(baro['air_pressure'].loc['2020-05-15':'2020-10-03']/9.81/10).air_pressure,
              lw=0.3, ls='-', marker=None, label='Barologger')
plt.legend()
plt.show()
# let's plot barologger, DWD and Dittersdorf level 1
# mpl is too stupid to make a simple plot, so just close the plot and we plot again. ain't nobody got time for that
# we can see that the level logger is higher than air pressure from DWD/barologger. This is impossible,
# since level logger should be hydrostatic pressure + air pressure

plt.plot_date(pd.DataFrame(baro['air_pressure'].loc['2020-05-28':'2020-07-31']/9.81/10).index,
              pd.DataFrame(baro['air_pressure'].loc['2020-05-28':'2020-07-31']/9.81/10).air_pressure,
              lw=0.3, ls='-', marker=None, label='Barologger')
plt.plot_date(pd.DataFrame(dfs.loc['2020-05-28':'2020-07-31']).index,
              pd.DataFrame(dfs.loc['2020-05-28':'2020-07-31']).level, lw=0.3, ls='-',
              marker=None, label='Dittersdorf l1')
plt.plot_date(pd.DataFrame(dwd['air_pressure'].loc['2020-05-28':'2020-07-31']/9.81/10).index,
              pd.DataFrame(dwd['air_pressure'].loc['2020-05-28':'2020-07-31']/9.81/10).air_pressure,
              lw=0.3, ls='-', marker=None, label='DWD')



plt.show()
plt.plot_date(pd.DataFrame(baro['air_pressure'].loc['2020-05-28':'2020-07-31']/9.81/10).index,
              pd.DataFrame(baro['air_pressure'].loc['2020-05-28':'2020-07-31']/9.81/10).air_pressure,
              lw=0.3, ls='-', marker=None, label='Barologger')
plt.plot_date(pd.DataFrame(dfs.loc['2020-05-28':'2020-07-31']).index,
              pd.DataFrame(dfs.loc['2020-05-28':'2020-07-31']).level, lw=0.3, ls='-',
              marker=None, label='Dittersdorf l1')
plt.plot_date(pd.DataFrame(dwd['air_pressure'].loc['2020-05-28':'2020-07-31']/9.81/10).index,
              pd.DataFrame(dwd['air_pressure'].loc['2020-05-28':'2020-07-31']/9.81/10).air_pressure,
              lw=0.3, ls='-', marker=None, label='DWD')
plt.legend()
plt.show()


# calculate the water levels based on barologger

baro = baro.loc[dfs.index[0]:dfs.index[-1]].resample('5min').mean()
p_water = (1000 * 9.81 * dfs['level']) - baro['air_pressure']*100
dfs['water_level'] = p_water / (1000 * 9.81)
dfs['water_level'].plot()
# let's just convert air pressure to mWS instead of converting mWS to pressure -> obviously same result
dfs['new_level'] = dfs.level-pd.DataFrame(baro['air_pressure']/9.81/10).air_pressure
dfs['new_level'] = dfs['new_level'] / (1000 * 9.81) * 10000
dfs['new_level'].plot()


# conclusions
# 1) The logger data at Ditterdorfer Bach is corrupt for the given time period
# 2) It doesn't really matter if we use the barologger or the deviation between DWD and barologger