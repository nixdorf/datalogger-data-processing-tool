#!/usr/bin/env python3.5
"""This script calculates the hydraulic diameter based on XYZ coordinates of a river profile as input

known issues: target = 0.5 for m2 -> ZeroDivisionError - fixed
"""

import numpy as np
from numpy.polynomial.polynomial import polyfit
import pandas as pd
from sympy import Point, Line
from shapely.geometry import Point as Pnt, LineString
from scipy import interpolate
import math
from functools import reduce
import operator


def create_profile(filename):
    """Creates a stream profile based on measurements.
    """
    df = pd.read_csv(filename, sep='\t', skiprows=3)

    pd.options.mode.chained_assignment = None  # disable copy warning

    # set the deepest point to zero to obtain heights
    deepest_point = min(df['Elevation'])
    df['Elevation'] += abs(deepest_point)

    # save stream edge point for catching error later
    lower_edge = min([max(df['Elevation'].loc[:df['Elevation'].idxmin()]),
                      max(df['Elevation'].loc[df['Elevation'].idxmin():])])

    easting = df['Easting']
    northing = df['Northing']

    # linear fit
    b, m = polyfit(easting, northing, 1)

    # create tuples for original points, define range of x for func
    points = list(zip(easting, northing))
    range_x = (min(easting) - 1, max(easting) + 1)

    # projection of points to line
    # create line
    g0 = Point(range_x[0], m * range_x[0] + b, evaluate=False)
    g1 = Point(range_x[1], m * range_x[1] + b, evaluate=False)
    l1 = Line(g0, g1)

    # create new columns for projected points
    df["proj_East"] = np.nan
    df["proj_North"] = np.nan

    # project points on regression line
    for num, point in enumerate(points):
        p0 = Point(point[0], point[1], evaluate=False)
        p1 = l1.projection(p0)
        df['proj_East'][num] = p1[0]
        df['proj_North'][num] = p1[1]

    # calculate distances between NE coords
    distances = [0]
    for i in range(0, len(df.index) - 1):
        a = np.array((df['proj_East'][i], df['proj_North'][i]))
        b = np.array((df['proj_East'][i + 1], df['proj_North'][i + 1]))
        distances.append(distances[i] + np.linalg.norm(a - b))

    coords = list(zip(distances, df['Elevation']))

    # interpolate elevation by 1 cm steps
    f = interpolate.interp1d(distances, df['Elevation'])
    dist_new = np.arange(distances[0], distances[-1], 0.01)
    elev_new = f(dist_new)

    parameter = [coords, dist_new, elev_new, distances, lower_edge]

    return parameter


def wetted_peri(parameter, target):
    """Calculates the wetted perimeter for flow calculation
    """




    coords, dist_new, elev_new, distances, lower_edge = parameter

    # catch error when water level is higher than lower edge of profile or data is NaN
    if target > lower_edge or np.isnan(target):
        d_h = np.nan
        return d_h

    # Calculate the wetted perimeter P
    # get intersections points between profile and water level
    level_line = LineString([Pnt(0, target), Pnt(max(distances) + 1, target)])
    lines = []
    intersections = []
    for i in range(0, len(coords) - 1):
        tmp_line = LineString([(coords[i][0], coords[i][1]), (coords[i + 1][0], coords[i + 1][1])])
        lines.append(tmp_line)
        intersections.append(level_line.intersection(tmp_line))

    intersections = [(i.xy[0][0], i.xy[1][0]) for i in intersections if not i.is_empty]

    # create list for each pair of intersections, containing a list with measuring points between
    between = []
    for i in range(0, len(intersections) // 2):
        between.append([intersections[i * 2], intersections[i * 2 + 1]])
        for coord in coords:
            if intersections[i * 2][0] < coord[0] < intersections[i * 2 + 1][0]:
                between[i].insert(-1, coord)

    # wetted perimeter = sum of perimeter for each section,
    # i = section if more than 2 intersections, j = coordinate in section [0/1] = X/Y
    peri = 0
    for i in range(0, len(between)):
        for j in range(0, len(between[i]) - 1):
            peri += math.sqrt(
                (between[i][j + 1][0] - between[i][j][0]) ** 2 + (between[i][j + 1][1] - between[i][j][1]) ** 2)

    # calculating the area with shoelace formula
    area = []
    # for each subarea
    for b in between:
        # order points clockwise, required for shoelace
        center = tuple(map(operator.truediv, reduce(lambda x, y: map(operator.add, x, y), b), [len(b)] * 2))
        between_clockwise = sorted(b, key=lambda b:
                            (-135 - math.degrees(math.atan2(*tuple(map(operator.sub, b, center))[::-1]))) % 360)
        # create list of x and y out of coordinate tuple
        xy_list = list(zip(*between_clockwise))
        # shoelace formula
        subarea = 0.5 * np.abs(np.dot(xy_list[0], np.roll(xy_list[1], 1)) - np.dot(xy_list[1], np.roll(xy_list[0], 1)))
        area.append(subarea)
    area = sum(area)

    # calculate the hydraulic diameter
    d_h = 4 * area / peri  # D_h = 4A/P
    if np.isnan(d_h):
        d_h = 0
    return d_h, area


def calc_diameter(ID, log):
    print('Calculating hydraulic diameters. This may take some minutes...', end='')
    file = 'input_data/Profiles/profile_{}.csv'.format(ID)

    log.index = pd.to_datetime(log.index, format='%Y%m%d %H:%M:%S')
    log['hyd_dia'] = np.nan
    log['area'] = np.nan
    params = create_profile(file)
    log[['hyd_dia', 'area']] = log['level'].apply(lambda x: pd.Series(wetted_peri(params, x)))

    print('OK\n')
    return log
